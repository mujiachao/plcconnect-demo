package com.jtexplorer.controller;

import com.jtexplorer.service.OPCUAService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 轮播图设置 接口类
 *
 * @author xu.wang
 * @since 2021-07-29
 */
@Slf4j
@RestController
@SuppressWarnings("SpringJavaAutowiringInspection")
@RequestMapping(value = "/test")
public class TestController {

    @Resource
    private OPCUAService opcuaService;

    @GetMapping(value = "/createClient")
    public void createClient(){
        opcuaService.createClient();
    }

    @GetMapping(value = "/createSubscription")
    public void createSubscription(){
        opcuaService.createSubscription();
    }


    /**
     * 表单提交 Date类型数据绑定
     *
     * @param binder 网页数据绑定
     * @see [类、类#方法、类#成员]
     */
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
}