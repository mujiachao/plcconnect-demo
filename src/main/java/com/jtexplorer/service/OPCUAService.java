package com.jtexplorer.service;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;

public interface OPCUAService {

    /**
     * 创建opcua客户端
     * @return
     */
    OpcUaClient createClient();

    /**
     * 创建订阅
     */
    void createSubscription();

}
